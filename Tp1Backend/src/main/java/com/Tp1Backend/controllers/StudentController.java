package com.Tp1Backend.controllers;

import com.Tp1Backend.models.Student;
import com.Tp1Backend.repositories.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api")
public class StudentController {
    private final StudentRepository studentRepository;

    @GetMapping("/load_students")
    public List<Student> findAllStudents() {
        return studentRepository.findAllStudents();
    }

    @PostMapping("/register_student")
    public boolean registerStudent(@RequestBody Student student) {
        if (student != null) {
            studentRepository.saveAndFlush(student);
            return true;
        }
        return false;
    }

    @DeleteMapping("/delete_student/{id}")
    public boolean deleteStudent(@PathVariable("id") Long id) {
        if (id != null) {
            Student found = studentRepository.findById(id).orElse(null);
            if (found != null) {
                studentRepository.delete(found);
                return true;
            }
        }
        return false;
    }
}
